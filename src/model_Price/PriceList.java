package model_Price;

import java.util.ArrayList;

import model_Items.Category;
import model_Items.Item;

public class PriceList {

    private String                name;
    private ArrayList<ItemPrices> itemPrices = new ArrayList<>();
    private boolean               barList    = false;
    private boolean               rentList   = false;

    public PriceList(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isBar() {
        return barList;
    }

    public void setIsBar(boolean b) {
        barList = b;
    }

    public boolean isRental() {
        return rentList;
    }

    public void setIsRental(boolean b) {
        rentList = b;
    }

    public double getPrice(int i, VALUTA_TYPE type) {
        return itemPrices.get(i).getPrice(type);
    }

    public double getPrice(Item e, VALUTA_TYPE type) {
        for (ItemPrices i : itemPrices) {
            if (i.getItem() == e) {
                return i.getPrice(type);
            }
        }
        try {
            throw new Exception(e.getName() + " is not present in the " + name + " list.");
        } catch (Exception e1) {

            e1.printStackTrace();
        }
        return 0;

    }

    public ArrayList<ItemPrices> getList() {
        return itemPrices;
    }

    public void addPrices(ItemPrices prices) {
        if (!itemPrices.contains(prices)) {
            itemPrices.add(prices);
        } else {

            try {
                throw new Exception("Prices for " + prices.getItem().getName() + ", already present in pricelist");
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public ItemPrices getPrices(Item i) {
        for (ItemPrices ip : itemPrices) {
            if (ip.getItem().equals(i)) {
                return ip;
            }
        }
        return null;
    }

    public ArrayList<ItemPrices> getItemsByCategory(Category category) {
        ArrayList<ItemPrices> temp = new ArrayList<>();
        for (ItemPrices i : itemPrices) {
            if (i.getItem().getCategory() == category) {
                temp.add(i);
            }
        }
        return temp;
    }

    @Override
    public String toString() {
        return name;
    }

}

package model_Price;

import java.util.LinkedHashMap;

import model_Items.Item;
import model_Items.ItemType;

public class ItemPrices {

    private Item                               item;

    private LinkedHashMap<VALUTA_TYPE, Double> prices = new LinkedHashMap<>();

    public ItemPrices(Item item) {
        this.item = item;
    }

    @SuppressWarnings("unchecked")
    public ItemPrices(ItemPrices itemPrices) {
        this(itemPrices.getItem());
        this.prices = (LinkedHashMap<VALUTA_TYPE, Double>) itemPrices.getList().clone();
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item i) {
        this.item = i;
    }

    @SuppressWarnings("cast")
    public double getPrice(VALUTA_TYPE type) {
        if (prices.get(type) != null) {
            return prices.get(type);
        }
        return 0;

    }

    public void setPrice(VALUTA_TYPE type, double amount) {
        prices.remove(type);
        prices.put(type, amount);
    }

    public void addPrice(VALUTA_TYPE type, double amount) {
        if (!prices.containsKey(type)) {
            prices.put(type, amount);
        } else {
            try {
                throw new Exception("Price already set for the type " + type.name());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public boolean containsPrice(VALUTA_TYPE type) {
        if (prices.containsKey(type)) {
            return true;
        }
        return false;
    }

    public void removePrice(VALUTA_TYPE type) {
        if (prices.containsKey(type)) {
            prices.remove(type);
        } else {
            throw new NullPointerException("Ingen Pris for typen " + getType() + " er sat for " + getItem());
        }

    }

    public ItemType getType() {
        return item.getItemType();
    }

    private LinkedHashMap<VALUTA_TYPE, Double> getList() {
        return prices;
    }

    @Override
    public String toString() {
        return item.toString() + ": " + getPrice(VALUTA_TYPE.DKK_Normal) + " " + VALUTA_TYPE.DKK_Normal;
    }

}

package model_Price;

import storage.Storage;

public enum VALUTA_TYPE {

    //@formatter:off
    DKK_Normal("DKK"),
    VOUCHER_MARK("Klip"),
    EURO("EUR"),
    US_DOLLARS("USD"),
    BRITISH_POUNDS("GBP")
    ;
    //@formatter:on

    private final String name;

    private VALUTA_TYPE(String name) {
        this.name = name;
        Storage.getInstance().addValuta(this);
    }

    @Override
    public String toString() {
        return name;
    }

}

package service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import model_Items.Basket;
import model_Items.Category;
import model_Items.DraftBeerSystem;
import model_Items.Item;
import model_Items.ItemType;
import model_Items.Keg;
import model_Items.Tour;
import model_Order.ActiveOrder;
import model_Order.Order;
import model_Order.OrderList;
import model_Price.ItemPrices;
import model_Price.PriceList;
import model_Price.VALUTA_TYPE;
import storage.Storage;

public class Service {

    static PriceList       BarPrices      = new PriceList("Bar Priser");
    static PriceList       ShopPrices     = new PriceList("Butiks Priser");
    static PriceList       RentPrices     = new PriceList("RentPrices");
    static PriceList       FestivalPrices = new PriceList("Festival Priser");

    private static Service service        = Service.getInstance();
    private static Storage storage        = Storage.getInstance();

    // -------------- Init ---------------------

    private Service() {
    }

    public static Service getInstance() {
        if (service == null) {
            service = new Service();
        }
        return service;
    }

    public void init() {

        // Festival Prices added for future possible expansion to new sales
        // situation
        storage.addPricesList(BarPrices);
        storage.addPricesList(ShopPrices);
        storage.addPricesList(RentPrices);
        storage.addPricesList(FestivalPrices);

        BarPrices.setIsBar(true);
        RentPrices.setIsRental(true);

        categoryAndBeersCreator();

        // Bar Items Creator
        Item_CreatorDKK("Chips", storage.getCategory(ItemType.SNACKS), 10, BarPrices);
        Item_CreatorDKK("Peanuts", storage.getCategory(ItemType.SNACKS), 10, BarPrices);
        Item_CreatorDKK("Æblebrus", storage.getCategory(ItemType.NonALCO), 15, BarPrices);
        Item_CreatorDKK("Cola", storage.getCategory(ItemType.NonALCO), 15, BarPrices);
        Item_CreatorDKK("Nikoline", storage.getCategory(ItemType.NonALCO), 15, BarPrices);
        Item_CreatorDKK("7-Up", storage.getCategory(ItemType.NonALCO), 15, BarPrices);
        Item_CreatorDKK("Vand", storage.getCategory(ItemType.NonALCO), 10, BarPrices);

        // Spirit Creator
        Item_CreatorDKK_SamePrice("Spirit of Aarhus", storage.getCategory(ItemType.SPIRIT), 300);
        Item_CreatorDKK_SamePrice("SOA med pind", storage.getCategory(ItemType.SPIRIT), 350);
        Item_CreatorDKK_SamePrice("Whisky", storage.getCategory(ItemType.SPIRIT), 500);
        Item_CreatorDKK_SamePrice("Liquor of Aarhus", storage.getCategory(ItemType.SPIRIT), 175);

        // Keg and Keg_Extra Creator
        Keg_CreatorDKK("Klosterbryg", storage.getCategory(ItemType.KEG), 775, 20, ShopPrices);
        Keg_CreatorDKK("Jazz Classic", storage.getCategory(ItemType.KEG), 625, 25, ShopPrices);
        Keg_CreatorDKK("Extra Pilsner", storage.getCategory(ItemType.KEG), 575, 25, ShopPrices);
        Keg_CreatorDKK("Celebration", storage.getCategory(ItemType.KEG), 775, 20, ShopPrices);
        Keg_CreatorDKK("Blondie", storage.getCategory(ItemType.KEG), 775, 25, ShopPrices);
        Keg_CreatorDKK("Forårsbryg", storage.getCategory(ItemType.KEG), 775, 20, ShopPrices);
        Keg_CreatorDKK("India Pale Ale", storage.getCategory(ItemType.KEG), 775, 20, ShopPrices);
        Keg_CreatorDKK("Julebryg", storage.getCategory(ItemType.KEG), 775, 20, ShopPrices);
        Keg_CreatorDKK("Imperial Stout", storage.getCategory(ItemType.KEG), 775, 20, ShopPrices);
        Item_CreatorDKK("Pant, Fustage", storage.getCategory(ItemType.KEG_EXTRA), 200, ShopPrices);
        Item_CreatorDKK("Krus til anlæg", storage.getCategory(ItemType.KEG_EXTRA), 60, ShopPrices);
        Item_CreatorDKK("Levering til Anlæg", storage.getCategory(ItemType.KEG_EXTRA), 500, ShopPrices);

        // Carbon Creator
        Item_CreatorDKK_SamePrice("Kulsyre, 6 kg.", storage.getCategory(ItemType.CARBON), 400);
        Item_CreatorDKK_SamePrice("Pant, Kulsyre", storage.getCategory(ItemType.CARBON), 1000);

        // Malt Creator
        Item_CreatorDKK("Malt, 25 kg. sæk", storage.getCategory(ItemType.MALT), 300, ShopPrices);

        // Wearable Creator
        Item_CreatorDKK_SamePrice("T-shirt", storage.getCategory(ItemType.WEARABLE), 70);
        Item_CreatorDKK_SamePrice("Polo", storage.getCategory(ItemType.WEARABLE), 100);
        Item_CreatorDKK_SamePrice("Cap", storage.getCategory(ItemType.WEARABLE), 30);

        DBS_CreatorDKK("1-hane", 1, 250);
        DBS_CreatorDKK("2-haner", 2, 400);
        DBS_CreatorDKK("Bar med flere haner", 3, 500);

        // Glass Creator
        Item_CreatorDKK("Glas", storage.getCategory(ItemType.GLASS), 15, ShopPrices);

        // Basket Creator
        Basket_CreatorDKK("Gaveæske", 2, 2, 100);
        Basket_CreatorDKK("Gaveæske", 4, 130);
        Basket_CreatorDKK("Trækasse", 6, 240);
        Basket_CreatorDKK("Gavekurv", 6, 2, 250);
        Basket_CreatorDKK("Trækasse", 6, 6, 290);
        Basket_CreatorDKK("Trækasse", 12, 390);
        Basket_CreatorDKK("Papkasse", 12, 360);

        // Voucher
        Item_CreatorDKK_SamePrice("Klippekort", storage.getCategory(ItemType.VOUCHER), 100);

        // Tour Creator
        Tour t = Tour_CreatorDKK("Rundvisning, Voksen", 100);
        t.setAdult();
        t = Tour_CreatorDKK("Rundvisning, Studerende", 80);
        t.setStudent();
        t = Tour_CreatorDKK("Rundvisning, barn", 70);
        t.setChild();

        PriceList p = BarPrices;

        Order o = new Order();

        OrderList o1 = new OrderList(p.getList().get(1), 2);
        OrderList o2 = new OrderList(p.getList().get(3), 5);
        OrderList o3 = new OrderList(p.getItemsByCategory(storage.getCategory(ItemType.SNACKS)).get(1));

        o.addOrder(o1);
        o.addOrder(o2);
        o.addOrder(o3);

        new ActiveOrder(o, LocalDateTime.now());

    }

    public Basket Basket_CreatorDKK(String basketType, int beerAmount, int glassAmount, double price) {
        Basket b = Basket_CreatorDKK(basketType, beerAmount, price);
        b.setName(basketType + " m. " + beerAmount + " øl, " + glassAmount + " glas");
        b.setGlassAmount(glassAmount);

        return b;

    }

    public Keg Keg_CreatorDKK(String name, Category category, double price, int size, PriceList priceList) {
        ItemPrices i = Item_CreatorDKK(name, category, price, priceList);
        Keg k = new Keg(i.getItem(), size);
        i.setItem(k);

        return k;
    }

    public Basket Basket_CreatorDKK(String basketType, int beerAmount, double price) {
        Basket b = new Basket(basketType + " m. " + beerAmount + " øl, ", storage.getCategory(ItemType.BASKET),
                beerAmount);

        storage.addItem(b);
        ItemPrices prices = new ItemPrices(b);
        prices.addPrice(VALUTA_TYPE.DKK_Normal, price);
        BarPrices.addPrices(prices);
        ShopPrices.addPrices(prices);
        return b;
    }

    public DraftBeerSystem DBS_CreatorDKK(String name, int tabsAmount, double price) {
        DraftBeerSystem DBS = new DraftBeerSystem(name, tabsAmount, storage.getCategory(ItemType.DBS));
        storage.addDBSList(DBS);
        ItemPrices prices = new ItemPrices(DBS);
        prices.addPrice(VALUTA_TYPE.DKK_Normal, price);
        RentPrices.addPrices(prices);
        return DBS;
    }

    /*
     * Adds the itemPrices to Shop- and BarPrice lists
     */
    public ItemPrices Item_CreatorDKK_SamePrice(String name, Category category, double price) {
        ItemPrices prices = Item_CreatorDKK(name, category, price);
        BarPrices.addPrices(prices);
        ShopPrices.addPrices(prices);
        return prices;

    }

    public ItemPrices Item_CreatorDKK(String name, Category category, double price, PriceList list) {
        ItemPrices prices = Item_CreatorDKK(name, category, price);
        list.addPrices(prices);
        return prices;

    }

    private ItemPrices Item_CreatorDKK(String name, Category category, double price) {
        Item e = new Item(name, category);

        storage.addItem(e);
        ItemPrices prices = new ItemPrices(e);

        prices.addPrice(VALUTA_TYPE.DKK_Normal, price);
        return prices;

    }

    public Tour Tour_CreatorDKK(String name, double price) {
        ItemPrices i = Item_CreatorDKK(name, storage.getCategory(ItemType.TOUR), price, ShopPrices);
        Tour t = new Tour(i.getItem());
        i.setItem(t);
        return t;
    }

    public Category categoryCreator(String name, ItemType type) {
        Category temp = new Category(name, type);
        storage.addCategory(temp);
        return temp;
    }

    public void categoryAndBeersCreator() {

        Category BB = categoryCreator("Flaskeøl", ItemType.BEER);
        BB.setVoucherAmount(2);
        Category DB = categoryCreator("Fadøl", ItemType.BEER);
        DB.setVoucherAmount(2);

        categoryCreator("Fustager", ItemType.KEG);
        categoryCreator("Fustage Tilbehør", ItemType.KEG_EXTRA);
        categoryCreator("Glas", ItemType.GLASS);
        Category snacks = categoryCreator("Snacks", ItemType.SNACKS);
        snacks.setVoucherAmount(1);

        categoryCreator("Spiritus", ItemType.SPIRIT);
        categoryCreator("Forfriskninger", ItemType.NonALCO);
        categoryCreator("Beklædning", ItemType.WEARABLE);
        categoryCreator("Rundvisning", ItemType.TOUR);
        categoryCreator("Maltsække", ItemType.MALT);
        categoryCreator("Kurv", ItemType.BASKET);
        categoryCreator("Kulsyre", ItemType.CARBON);
        categoryCreator("Klippekort", ItemType.VOUCHER);
        categoryCreator("Anlæg", ItemType.DBS);

        ArrayList<String> BB_List = BB_List();
        for (String s : BB_List) {
            Item_CreatorDKK(s, BB, 50, BarPrices);
        }

        // ShopePrices - BottleBeer
        for (String s : BB_List) {
            Item_CreatorDKK(s, BB, 36, ShopPrices);
        }

        // BarPrices - DraftBeer
        ArrayList<String> DB_List = DB_List();
        for (String s : DB_List) {
            Item_CreatorDKK(s, DB, 30, BarPrices);
        }
    }

    // ------------- BeerName:String Lists -----------------

    public static ArrayList<String> BB_List() {
        ArrayList<String> list = new ArrayList<>();

        list.add("Klosterbryg");
        list.add("Sweet Georgia Brown");
        list.add("Extra Pilsner");
        list.add("Celebration");
        list.add("Blondie");
        list.add("Forårsbryg");
        list.add("India Pale Ale");
        list.add("Julebryg");
        list.add("Juletønden");
        list.add("Old Strong Ale");
        list.add("Fregatten Jylland");
        list.add("Imperial Stout");
        list.add("Tribute");

        return list;
    }

    public static ArrayList<String> DB_List() {
        ArrayList<String> list = new ArrayList<>();

        list.add("Klosterbryg, 40 cl.");
        list.add("Jazz Classic, 40 cl.");
        list.add("Extra Pilsner, 40 cl.");
        list.add("Celebration, 40 cl.");
        list.add("Blondie, 40 cl.");
        list.add("Forårsbryg, 40 cl.");
        list.add("India Pale Ale, 40 cl.");
        list.add("Julebryg, 40 cl.");
        list.add("Imperial Stout, 40 cl.");
        list.add("Special, 40 cl.");

        return list;
    }

    // -------------------------- Order Methods -----------------------

    public ActiveOrder orderPayed(Order o, LocalDateTime date) {
        ActiveOrder ao = new ActiveOrder(o, date);
        storage.saveOrder(ao);
        return ao;
    }

    public ActiveOrder orderPayed(ActiveOrder ao) {
        storage.saveOrder(ao);
        return ao;
    }

    public Order createOrderListToOrder(ItemPrices itemPrices, int amount, Order o) {
        o.addOrder(itemPrices, amount);

        return o;

    }

    public void removeItemFromOrder(OrderList oL, Order o) throws Exception {
        if (o.containsOrderList(oL)) {
            o.removeOrder(oL);
        } else {
            throw new Exception("No such OrderList is present in the current order");
        }

    }

    public void removeAmountFromOrderList(OrderList o, int removeAmount) {
        if (o.getAmount() - removeAmount < 0) {
            try {
                throw new Exception("The wanted amount removed is greater than the current amount");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            o.setAmount(o.getAmount() - removeAmount);
        }
    }

    public void removeOneAmountFromOrderList(OrderList o) {
        removeAmountFromOrderList(o, 1);
    }

    public ArrayList<ItemPrices> getBarIPbyCategory(Category category) {
        ArrayList<ItemPrices> temp = new ArrayList<>();
        for (ItemPrices i : getBarPriceList().getList()) {
            if (i.getItem().getCategory() == category) {
                temp.add(i);
            }
        }
        return temp;
    }

    // -------------- Rental Methods ------------

    public ArrayList<DraftBeerSystem> getRentalEquipment() {
        return storage.getDBSList();
    }

    public PriceList getRentalPriceList() {
        for (PriceList p : storage.getPricesList()) {
            if (p.isRental()) {
                return p;
            }
        }
        return null;
    }

    // ------ Misc List return methods --------

    public ArrayList<ItemPrices> getShopIPbyCategory(PriceList priceList, Category category) {
        ArrayList<ItemPrices> temp = new ArrayList<>();
        for (ItemPrices i : priceList.getList()) {
            if (i.getItem().getCategory() == category) {
                temp.add(i);
            }
        }
        return temp;
    }

    public ArrayList<ItemPrices> getFullIPList() {
        ArrayList<ItemPrices> temp = new ArrayList<>();
        for (ItemPrices i : getBarPriceList().getList()) {
            temp.add(i);
        }
        return temp;
    }

    public PriceList getBarPriceList() {
        for (PriceList p : storage.getPricesList()) {
            if (p.isBar()) {
                return p;
            }
        }
        return null;
    }

    private ArrayList<ItemPrices> getTourPrices() {
        ArrayList<ItemPrices> temp = new ArrayList<>();
        for (PriceList p : getNonBarPriceList()) {
            for (ItemPrices i : p.getList()) {
                if (i.getType() == ItemType.TOUR) {
                    temp.add(i);
                }
            }
        }
        return temp;
    }

    public ArrayList<PriceList> getNonBarPriceList() {
        ArrayList<PriceList> temp = new ArrayList<>();
        for (PriceList p : storage.getPricesList()) {
            if (!p.isBar() && !p.isRental()) {
                temp.add(p);
            }
        }
        return temp;

    }

    public ArrayList<Category> getNonEmptyCategories(PriceList list) {
        ArrayList<Category> temp = new ArrayList<>();
        for (Category c : storage.getCategories()) {
            int ph = list.getItemsByCategory(c).size();
            if (ph > 0 && c.getType() != ItemType.TOUR) {
                temp.add(c);
            }
        }

        return temp;
    }

    // --------- Statistics Methods -------------

    public ArrayList<ActiveOrder> sortOrderByDate() {
        ArrayList<ActiveOrder> orders = storage.getSavedActiveOrders();
        Collections.sort(orders, new Comparator<ActiveOrder>() {
            @Override
            public int compare(ActiveOrder m1, ActiveOrder m2) {
                return m1.getDate().compareTo(m2.getDate());
            }
        });
        return orders;
    }

    public ArrayList<ActiveOrder> getSalesByDay(LocalDate date) {
        ArrayList<ActiveOrder> temp = new ArrayList<>();
        for (ActiveOrder a : sortOrderByDate()) {
            if (a.getDate().toLocalDate().equals(date)) {
                temp.add(a);
            }
        }
        return temp;
    }

    // ------------- DraftBeerSystem Methods -------------

    public DraftBeerSystem getFreeDBS(int amount) {
        for (DraftBeerSystem DBS : storage.getDBSList()) {
            if (DBS.getTabs() == amount && !DBS.isInUse()) {
                return DBS;
            }
        }
        return null;
    }

    // ------------ Tour Methods --------------

    public Order placeTourOrder(int adultAmount, int studentAmount, int childAmount, LocalDate date,
            boolean inEvening) {
        Order o = new Order();
        for (ItemPrices iP : getTourPrices()) {
            ItemPrices i = new ItemPrices(iP);
            Tour t = new Tour(i.getItem());
            t.setDate(date);
            t.setInEvening(inEvening);
            i.setItem(t);
            if (t.isAdult() && adultAmount > 0) {
                OrderList ol = new OrderList(i, adultAmount);
                o.addOrder(ol);
            } else if (t.isChild() && childAmount > 0) {
                OrderList ol = new OrderList(i, childAmount);
                o.addOrder(ol);
            } else if (t.isStudent() && studentAmount > 0) {
                OrderList ol = new OrderList(i, studentAmount);
                o.addOrder(ol);
            }

        }

        return o;
    }

    // ------------- Order Cleaner ----------------------
    public void orderCleaner(Order o) {
        ArrayList<OrderList> temp = new ArrayList<>();
        if (!o.getOrderList().isEmpty()) {
            for (OrderList ol : o.getOrderList()) {
                if (ol.getAmount() <= 0) {
                    temp.add(ol);
                }
            }
            for (OrderList i : temp) {
                o.removeOrder(i);
            }
        }
    }

}

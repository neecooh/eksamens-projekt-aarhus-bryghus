package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import model_Items.Category;
import model_Items.Item;
import model_Items.ItemType;
import model_Order.Order;
import model_Order.OrderList;
import model_Price.ItemPrices;
import service.Service;
import storage.Storage;

public class TestService {
    Storage            storage;
    Service            service;
    private Order      o;
    private Item       i;
    private Category   c;
    private ItemPrices IP;
    private OrderList  ol;

    @Before
    public void setup() {
        c = new Category("TestCategory", ItemType.BEER);
        i = new Item("TestØl", c);
        IP = new ItemPrices(i);
        o = new Order();
        ol = new OrderList(IP);

    }

    @Test
    public void testCreateOrderListToOrder() {
        ol = o.addOrder(IP, 1);
        assertTrue(o.getOrderList().contains(ol));
    }

    @Test
    public void testRemoveItemFromOrder() throws Exception {
        ol = o.addOrder(IP, 1);
        service.removeItemFromOrder(ol, o);
        assertTrue(o.getOrderList().isEmpty());
    }

    @Test
    public void testRemoveItemFromOrder2() throws Exception {
        double actualResult = 0;
        ol = o.addOrder(IP, 3);
        ol = o.addOrder(IP, 1);
        service.removeItemFromOrder(ol, o);
        actualResult = o.getOrderList().size();
        assertEquals(1, actualResult, 0.001);
    }

    @Test(expected = java.lang.Exception.class)
    public void testRemoveItemFromOrder3() throws Exception {
        double actualResult = 0;
        service.removeItemFromOrder(ol, o);
        actualResult = o.getOrderList().size();
        assertEquals(0, actualResult, 0.001);
    }

    @Test
    public void testRemoveAmountFromOrderList() throws Exception {
        double actualResult = 0;
        ol = o.addOrder(IP, 3);
        service.removeAmountFromOrderList(ol, 2);
        actualResult = ol.getAmount();
        assertEquals(1, actualResult, 0.001);
    }

    @Test
    public void testRemoveAmountFromOrderList2() throws Exception {
        double actualResult = 0;
        ol = o.addOrder(IP, 100);
        service.removeAmountFromOrderList(ol, 50);
        actualResult = ol.getAmount();
        assertEquals(50, actualResult, 0.001);
    }

    @Test(expected = java.lang.Exception.class)
    public void testRemoveAmountFromorderList3() throws Exception {
        double actualResult = 0;
        ol = o.addOrder(IP, 0);
        service.removeAmountFromOrderList(ol, 1);
        actualResult = ol.getAmount();
        assertEquals(0, actualResult, 0.001);
    }

    @Test
    public void testRemoveOneAmountFromOrderList() throws Exception {
        double actualResult = 0;
        ol = o.addOrder(IP, 2);
        service.removeOneAmountFromOrderList(ol);
        actualResult = ol.getAmount();
        assertEquals(1, actualResult, 0.001);
    }

    @Test
    public void testplaceTourOrder() {
        Order o = service.placeTourOrder(1, 0, 0, LocalDate.now(), true);
        assertTrue(o.getOrderList().contains(ol));
    }

    @Test
    public void testGetBarIPbyCategory() {
        ArrayList<ItemPrices> a = service.getBarIPbyCategory(c);
        assertTrue(a.contains(c));
    }

}
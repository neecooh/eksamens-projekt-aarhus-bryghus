package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import model_Items.Category;
import model_Items.Item;
import model_Items.ItemType;
import model_Order.Order;
import model_Order.OrderList;
import model_Price.ItemPrices;
import model_Price.VALUTA_TYPE;
import service.Service;
import storage.Storage;

public class TestOrder {
    Storage                      storage   = Storage.getInstance();
    Service                      service   = Service.getInstance();
    private Order                o;
    private Item                 i;
    private Category             c;
    private ItemPrices           ip;
    private OrderList            ol;

    private ArrayList<OrderList> orderList = new ArrayList<>();

    @Before
    public void setup() throws Exception {
        c = new Category("TestCategory", ItemType.BEER);
        i = new Item("TestØl", c);
        ip = new ItemPrices(i);
        ip.addPrice(VALUTA_TYPE.DKK_Normal, 100);
        o = new Order();
        ol = new OrderList(ip);

    }

    @Test
    public void TestaddOrder() {
        double actualResult = 0;
        ol = o.addOrder(ip, 1);
        for (int i = 0; i < o.getOrderList().size(); i++) {
            actualResult++;
        }
        assertEquals(1, actualResult, 0.001);
    }

    @Test
    public void TestAddOrder2() {
        double actualResult = 0;
        for (int i = 0; i < 2; i++) {
            ol = o.addOrder(ip, 1);
        }
        actualResult = o.getOrderList().size();
        assertEquals(2, actualResult, 0.001);
    }

    @Test
    public void TestremoveOrder() {
        ol = o.addOrder(ip, 1);
        assertTrue(o.removeOrder(ol));
    }

    @Test
    public void testRemoveOrder2() {
        double actualResult = 0;
        for (int i = 0; i < 10; i++) {
            ol = o.addOrder(ip, 1);
        }
        o.removeOrder(ol);
        actualResult = o.getOrderList().size();
        assertEquals(9, actualResult, 0.001);

    }

    @Test
    public void testRemoveOrder3() {
        double actualResult = 0;
        o.removeOrder(ol);
        actualResult = o.getOrderList().size();
        assertEquals(0, actualResult, 0.001);
    }

    @Test
    public void TestgetOrderList() {
        orderList.add(ol = (o.addOrder(ip, 1)));
        assertTrue(o.getOrderList().contains(ol));
    }

    @Test
    public void TestcontainsOrderList() {
        o.containsOrderList(ol);
    }

    @Test
    public void TestgetOrderListByItem() {
        o.addOrder(ol = (o.addOrder(ip, 1)));
        assertTrue(o.getOrderList().contains(o.getOrderListByItem(i)));
    }

    @Test
    public void testFullCost() {
        double actualResult = 0;
        o.addOrder(ip, 1);
        actualResult = o.fullCost();
        assertEquals(100, actualResult, 0.001);
    }

    @Test
    public void testFullCost2() {
        double actualResult = 0;
        o.addOrder(ip, 2);
        actualResult = o.fullCost();
        assertEquals(200, actualResult, 0.001);
    }

    @Test
    public void testFullCost3() {
        double actualResult = 0;
        o.addOrder(ip, 0);
        actualResult = o.fullCost();
        assertEquals(0, actualResult, 0.001);
    }

    @Test
    public void testFullCost4() throws Exception {
        double actualResult = 0;
        Item item = new Item("TestØl", c);
        ItemPrices itemprices = new ItemPrices(item);
        itemprices.addPrice(VALUTA_TYPE.DKK_Normal, -100);
        Order order = new Order();
        order.addOrder(itemprices, 1);
        actualResult = order.fullCost();
        assertEquals(-100, actualResult, 0.001);
    }

}

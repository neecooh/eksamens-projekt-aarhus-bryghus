package test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;

import org.junit.Before;
import org.junit.Test;

import model_Items.Category;
import model_Items.Item;
import model_Items.ItemType;
import model_Order.ActiveOrder;
import model_Order.Order;
import model_Price.ItemPrices;
import model_Price.VALUTA_TYPE;
import service.Service;
import storage.Storage;

public class TestActiveOrder {

    Storage               storage = Storage.getInstance();
    Service               service = Service.getInstance();
    private Order         o;
    private Item          i;
    private Category      c;
    private ItemPrices    IP;
    private ActiveOrder   ao;
    private LocalDateTime date    = LocalDateTime.now();

    @Before
    public void setup() throws Exception {
        c = new Category("TestCategory", ItemType.BEER);
        i = new Item("TestØl", c);
        IP = new ItemPrices(i);
        o = new Order();
        IP.addPrice(VALUTA_TYPE.DKK_Normal, 10);
        IP.addPrice(VALUTA_TYPE.VOUCHER_MARK, 2);
        o.addOrder(IP, 1);
        ao = new ActiveOrder(o, date);
    }

    @Test
    public void testgetPrice() {
        double actualResult = 0;
        actualResult = ao.getPrice();
        assertEquals(10, actualResult, 0.001);
    }

    @Test
    public void testpayItemWithVoucher() {
        int voucherAmountSpent = 2;
        int expectedRemainingAmount = 0;
        ao.payItemWithVoucher(voucherAmountSpent);
        // Counts Remaining cost
        int result = ao.getFullVoucherCost();
        assertEquals(expectedRemainingAmount, result, 0.001);
    }

    @Test
    public void testGetFullVoucherCost() {
        int expectedAmount = 2;
        assertEquals(expectedAmount, ao.getFullVoucherCost(), 0.001);
    }

    @Test
    public void testValutaConversion() {
        o.addOrder(IP, 1);

    }

    @Test
    public void TestCalculateDiscountdPrice() {
        int discountAmount = 25;
        double actualResult = ao.calculateDiscountedPrice(discountAmount);
        assertEquals(7.5, actualResult, 0.001);
    }
}

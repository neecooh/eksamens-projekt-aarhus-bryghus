package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import model_Items.Basket;
import model_Items.Category;
import model_Items.Item;
import model_Items.ItemType;
import model_Order.OrderList;
import model_Price.ItemPrices;
import model_Price.VALUTA_TYPE;
import service.Service;
import storage.Storage;

public class TestOrderList {
    Storage            storage = Storage.getInstance();
    Service            service = Service.getInstance();
    private Item       i;
    private Basket     b;
    private Category   c, bc;
    private ItemPrices ip, ipb;
    private OrderList  ol;

    @Before
    public void setup() throws Exception {
        c = new Category("TestCategory", ItemType.BEER);
        bc = new Category("TestBacket", ItemType.BASKET);
        i = new Item("TestØl", c);
        ip = new ItemPrices(i);
        ip.addPrice(VALUTA_TYPE.DKK_Normal, 100);
        ol = new OrderList(ip, 2);

    }

    @Test
    public void testGetPrice() {
        double expectedAmount = 200;
        assertEquals(expectedAmount, ol.getPrice(), 0.0001);
    }

    @Test
    public void testBasketNotFilled1() {
        b = new Basket("TestBasket", bc, 2);
        // Inadequate amount of beers added
        b.addBeer(i, 1);
        ipb = new ItemPrices(b);
        ol = new OrderList(ipb, 1);

        assertTrue(ol.basketNotFilled());
    }

    @Test
    public void testBacketNotFilled2() {
        b = new Basket("TestBasket", bc, 2);
        // Beer added to match basket size
        b.addBeer(i, 2);
        ipb = new ItemPrices(b);
        ol = new OrderList(ipb, 1);

        assertFalse(ol.basketNotFilled());
    }

}

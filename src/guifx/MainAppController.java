package guifx;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TabPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class MainAppController implements Initializable {

    // Add all the scenebuilder stuff!
    @FXML
    private Pane       layoutPane, fredagsbarPane;

    @FXML
    private Button     barSystemBtn, lagerSystemBtn, fredagsbarTabBtn, butikTabBtn, btnUdlej, btnMinus, btnPlus, btnAdd;

    @FXML
    private BorderPane borderPane;

    @FXML
    private ImageView  logo, logo1, background;

    @FXML
    private TabPane    tabPane;

    @FXML
    private Button     closePaneButton;

    // Field needed for the counterBar
    int                count = 0;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    @FXML
    private void barSystem(ActionEvent event) {

        try {
            FXMLLoader root = new FXMLLoader(getClass().getResource("/guifx/barSystem.fxml"));
            Parent root1 = (Parent) root.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root1));
            stage.setResizable(false);
            stage.centerOnScreen();
            stage.sizeToScene();
            System.out.println("Bar System GUI : Initialized");
            stage.showAndWait();

        } catch (IOException e) {
            System.out.println("Bar System GUI : Error: " + e + "Error cause: " + e.getCause());
            System.out.println("Bar System GUI : Error: " + e);
        }

    }

    // ------------- Initialize lagersystem GUI ------------
    @FXML
    private void lagerSystem(ActionEvent event) {
        try {
            FXMLLoader root = new FXMLLoader(getClass().getResource("/guifx/LagerSystem.fxml"));
            Parent root1 = (Parent) root.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root1));
            stage.setResizable(false);
            stage.centerOnScreen();
            stage.sizeToScene();
            System.out.println("Lager System GUI : Initialized");
            stage.showAndWait();

        } catch (IOException e) {
            System.out.println("Lager System GUI : Error: " + e + "Error cause: " + e.getCause());
        }
    }
    // ------------------------------------------------------

    @FXML
    private void closePane() {
        Stage stage = (Stage) closePaneButton.getScene().getWindow();
        stage.close();
    }

}

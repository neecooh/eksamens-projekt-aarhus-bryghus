package guifx;

import java.net.URL;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model_Items.Category;
import model_Items.DraftBeerSystem;
import model_Items.ItemType;
import model_Order.ActiveOrder;
import model_Price.ItemPrices;
import model_Price.PriceList;
import model_Price.VALUTA_TYPE;
import service.Service;
import storage.Storage;

public class LagerController implements Initializable {

    @FXML
    private TextField              txfFirstName, txfLastName, txfEmail, txfAddress, txfPostal, txfPhone, beerCount,
            glassCount, txfCreateName, txfCreatePrice, txfCreateCategory, txfCreateNameOnItem, txfCreatePriceOnItem;

    @FXML
    private ListView<String>       rentList;

    @FXML
    private ListView<ActiveOrder>  lwStatistics;

    @FXML
    private Button                 btnUdlej, btnCreateCategory, btnFewer1, btnMore1, btnFewer2, btnMore2,
            removeRent, btnBack, btnDeletePrice;

    @FXML
    private Label                  lblGlassAmount, lblBeerAmount;
    @FXML
    private DatePicker             fromDate, toDate, dtpStatistics;

    @FXML
    private TextArea               txaRentReceipt;

    @FXML
    private ChoiceBox<Category>    chbCreateItem;

    @FXML
    private ChoiceBox<ItemType>    chbCategoryType;

    @FXML
    private ChoiceBox<String>      chbChooseRentItem;

    @FXML
    private ChoiceBox<PriceList>   chbPriceList;

    @FXML
    private ChoiceBox<ItemPrices>  chbItemPrices;

    @FXML
    private ChoiceBox<VALUTA_TYPE> chbValutaOnItem;

    @FXML
    private ChoiceBox<PriceList>   chbCreatePricePriceList;

    private Service                service = Service.getInstance();

    private Storage                storage = Storage.getInstance();

    ArrayList<String>              rents   = new ArrayList<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        updateControls();
    }

    private void updateControls() {
        chbCreateItemCategories();
        rentList();
        getRentalEquipment();
        RentReceiptChange();
        chbCategoryType();
        chbPriceList();
        chbCreatePricePriceList();
        clearPriceChange();
    }

    private boolean notNull(ChoiceBox<?> c) {
        return c.getSelectionModel().getSelectedItem() != null;
    }

    @FXML
    private void btnBack() {
        Stage stage = (Stage) btnBack.getScene().getWindow();
        stage.close();
    }

    // --------------- DBS Rental Methods -----------------------

    @FXML
    private void fromDate() {
        fromDate.setOnAction((event) -> {
            RentReceiptChange();
        });
    }

    @FXML
    private void toDate() {
        toDate.setOnAction((event) -> {
            RentReceiptChange();
        });
    }

    @FXML
    private void getRentalEquipment() {
        chbChooseRentItem.getItems().add("1-hane");
        chbChooseRentItem.getItems().add("2-haner");
        chbChooseRentItem.getItems().add("Bar med flere haner");
        chbChooseRentItem.getSelectionModel().selectFirst();
        chbChooseRentItem.setOnAction((event) -> {
            RentReceiptChange();
            DBSAvaliable();
        });
    }

    private void DBSAvaliable() {
        if (service.getFreeDBS(chbChooseRentItem.getSelectionModel().getSelectedIndex() + 1) == null) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error");
            alert.setContentText("Intet anlæg med det antal haner er tilgængelig");
            alert.show();

            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            stage.setAlwaysOnTop(true);
            stage.toFront();
        }

    }

    // ------------------- Create/Alter Price Methods ---------------

    @FXML
    private void chbCreatePricePriceList() {
        chbCreatePricePriceList.getItems().setAll(storage.getPricesList());
        chbCreatePricePriceList.setOnAction((event) -> {
            if (notNull(chbCreatePricePriceList)) {
                chbItemPrices();
            } else {
                return;
            }
        });
    }

    private boolean checkAlterPriceFields() {
        boolean b = false;
        String alertMsg = "Information mangler i felterne:";
        Alert alert = new Alert(AlertType.ERROR);
        if (!notNull(chbValutaOnItem)) {
            alertMsg += " \"Valuta Type\"";
            b = true;
        }
        if (!notNull(chbItemPrices)) {
            if (b) {
                alertMsg += ",";
            }
            alertMsg += " \"Produkt priser\"";
            b = true;
        }
        if (txfCreateNameOnItem.getText() == null) {
            if (b) {
                alertMsg += ",";
            }
            alertMsg += " \"Produkt navn\"";
            b = true;
        }
        try {
            Double d = Double.parseDouble(txfCreatePriceOnItem.getText());
            if (d < 1) {
                alert.setAlertType(AlertType.ERROR);
                alert.setContentText("Prisen skal være højere end 0");
                alert.show();
                clearPriceChange();
                return true;
            }
        } catch (Exception e) {
            alert.setAlertType(AlertType.ERROR);
            alert.setContentText("Prisen skal skrives med tal");
            alert.show();
            clearPriceChange();
            return true;
        }
        if (txfCreatePriceOnItem.getText() == null) {
            if (b) {
                alertMsg += ",";
            }
            alertMsg += " \"Produkt pris\"";

            b = true;
        }
        if (b) {
            alert.setContentText(alertMsg);
            alert.show();
        }
        return b;
    }

    @FXML
    private void btnDeletePrice() {
        Alert alert = new Alert(AlertType.INFORMATION);
        ItemPrices temp = chbItemPrices.getSelectionModel().getSelectedItem();
        VALUTA_TYPE t = chbValutaOnItem.getSelectionModel().getSelectedItem();
        try {
            temp.removePrice(t);
            alert.setContentText(t + " prisen for " + temp.getItem() + " er nu fjernet.");

        } catch (NullPointerException e) {
            alert.setAlertType(AlertType.ERROR);
            alert.setContentText(e.getMessage());
            alert.show();

        }
        clearPriceChange();
    }

    @FXML
    private void btnCreatePrice() throws Exception {
        if (checkAlterPriceFields()) {
            updateControls();
            return;
        }
        Alert alert = new Alert(AlertType.INFORMATION);
        ItemPrices temp = chbItemPrices.getSelectionModel().getSelectedItem();
        VALUTA_TYPE t = chbValutaOnItem.getSelectionModel().getSelectedItem();
        Double d = Double.parseDouble(txfCreatePriceOnItem.getText());
        if (temp.getPrice(t) == d) {
            alert.setContentText("Ingen ændring for " + t + " prisen på " + temp.getItem() + " er sket.");
            alert.show();

        } else if (temp.getPrice(t) != 0) {
            temp.setPrice(t, d);
            alert.setContentText(t + " prisen for " + temp.getItem() + " er ændret til " + d);
            alert.show();

        } else {
            temp.addPrice(t, d);
            alert.setContentText(t + " pris er tilføjet til " + temp.getItem() + " med en værdi af " + d);
            alert.show();
        }
        updateControls();
    }

    private void clearPriceChange() {
        chbCreatePricePriceList.getSelectionModel().clearSelection();
        chbItemPrices.getSelectionModel().clearSelection();
        chbValutaOnItem.getSelectionModel().clearSelection();
        txfCreatePriceOnItem.clear();
        txfCreateNameOnItem.clear();
    }

    @FXML
    private void chbValutaOnItem() {
        chbValutaOnItem.getItems().setAll(storage.getValutas());
        chbValutaOnItem.setOnAction((event) -> {
            try {
                Double d = chbItemPrices.getSelectionModel().getSelectedItem()
                        .getPrice(chbValutaOnItem.getSelectionModel().getSelectedItem());
                if (notNull(chbItemPrices) && notNull(chbValutaOnItem) && !d.equals(null)) {
                    txfCreatePriceOnItem.setText("" + d);
                }
                if (d == 0) {
                    throw new NullPointerException();
                }

            } catch (NullPointerException e) {
                txfCreatePriceOnItem.clear();
                txfCreatePriceOnItem.setPromptText("Eks. 50 " + chbValutaOnItem.getSelectionModel().getSelectedItem());
            }

        });

    }

    @FXML
    private void chbItemPrices() {
        if (!notNull(chbCreatePricePriceList)) {
            return;
        }
        chbItemPrices.getItems().setAll(chbCreatePricePriceList.getSelectionModel().getSelectedItem().getList());
        chbItemPrices.getSelectionModel().selectFirst();

        txfCreateNameOnItem.setText(chbItemPrices.getSelectionModel().getSelectedItem().getItem().getName());
        chbValutaOnItem();
        chbItemPrices.setOnAction((event) -> {
            if (notNull(chbItemPrices)) {
                txfCreateNameOnItem.setText(chbItemPrices.getSelectionModel().getSelectedItem().getItem().getName());
            } else {
                return;
            }
        });

    }

    @FXML
    private void chbPriceList() {
        chbPriceList.getItems().setAll(storage.getPricesList());
        chbPriceList.getSelectionModel().selectFirst();
    }

    // Check for missing/wrong input during rental creation
    @FXML
    private void udlejBtn(ActionEvent event) {
        String errorString = "";

        // Checking if firstname is present
        String name = txfFirstName.getText().trim();
        if (name.length() == 0) {
            errorString += "Fornavn er påkrævet\n";
        }

        // Checking if lastname is present
        String lastname = txfLastName.getText().trim();
        if (lastname.length() == 0) {
            errorString += "\nEfternavn er påkrævet\n";
        }

        // Checking if address is present
        String address = txfAddress.getText().trim();
        if (address.length() == 0) {
            errorString += "\nAdresse er påkrævet\n";
        }

        // Checking if postal is present and a number.
        String postal = txfPostal.getText().trim();
        int postnr = -1;
        try {
            postnr = Integer.parseInt(txfPostal.getText().trim());
        } catch (NumberFormatException ex) {
        }

        if (postnr < 0) {
            errorString += "\nPostnummer skal være et tal\n";
        }

        if (postal.length() == 0) {
            errorString += "\nPostnummer er påkrævet\n";
        }

        // Checking that from/to date is present
        // and toDate is !before fromDate
        LocalDate fromDate = this.fromDate.getValue();
        if (fromDate == null) {
            errorString += "\nLeje dato skal være valgt\n";
        }

        LocalDate toDate = this.toDate.getValue();
        if (toDate == null) {
            errorString += "\nAfleveringsdato skal være valgt\n";
        }

        if (fromDate != null && toDate != null) {
            if ((fromDate.until(toDate, ChronoUnit.DAYS)) < 0) {
                errorString += "\nAfleveringsdatoen kan ikke være før leje datoen..\n";
            }
        }

        // If any errors added to errorString, creates alertwindow with all the
        // errors.
        if (errorString.length() != 0) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error");
            alert.setContentText(errorString);
            alert.show();

            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            stage.setAlwaysOnTop(true);
            stage.toFront();
            return;
        } else if (errorString.length() == 0) {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Success");
            alert.setContentText("Udlejning oprettet!");
            alert.show();
            createRent();
            rentList();

            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            stage.setAlwaysOnTop(true);
            stage.toFront();
            return;
        }
    }

    // -------------- Receipt updater ----------------

    @FXML
    private void RentReceiptChange() {
        String information = "";
        information += "Valgt udstyr: " + chbChooseRentItem.getSelectionModel().getSelectedItem() + "\n\n";
        information += "Navn: " + txfFirstName.getText().trim() + " " + txfLastName.getText().trim() + "\n\n";
        information += "Addresse: " + txfAddress.getText().trim() + "\n\n";
        information += "Postnr: " + txfPostal.getText().trim() + "\n\n";
        information += "Email: " + txfEmail.getText().trim().toLowerCase() + "\n\n";
        information += "Tlf. nr.: " + txfPhone.getText().trim() + "\n\n";
        if (fromDate.getValue() == null) {
            information += "Låne dag: Endnu ikke sat" + "\n\n";
        } else {
            information += "Låne dag: " + fromDate.getValue() + "\n\n";
        }
        if (toDate.getValue() == null) {
            information += "Afleveringsdato: Endnu ikke sat" + "\n\n";
        } else {
            information += "Afleveringsdato: " + toDate.getValue();
        }
        information += "\n";

        this.txaRentReceipt.setText(information);
    }

    // ----------- Create/Remove Rent Methods -------------------

    private void createRent() {
        String information = "Navn: " + txfFirstName.getText().trim() + " " + txfLastName.getText().trim()
                + " | Email: " + txfEmail.getText().trim().toLowerCase() +
                " | Tlfnr: " + txfPhone.getText().trim()
                + " | Udstyr: " + chbChooseRentItem.getSelectionModel().getSelectedItem()
                + " | Afleveringsdato: " + toDate.getValue()
                + " | Dage til indlevering: " + ChronoUnit.DAYS.between(LocalDate.now(), toDate.getValue());
        DraftBeerSystem DBS = service.getFreeDBS(chbChooseRentItem.getSelectionModel().getSelectedIndex() + 1);
        if (DBS == null) {
            System.out.println("No DBS avaliable");
        } else {
            DBS.setRenter(information);
            rents.add(information);
        }

    }

    @FXML
    private void removeRent() {
        if (rents.isEmpty() == false && rentList.getSelectionModel().getSelectedItem() != null) {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Indlevering af lånt udstyr");
            alert.setContentText("Udlån godkendt som indleveret.");
            alert.showAndWait();
            rents.remove(rentList.getSelectionModel().getSelectedIndex());
            rentList();
        }
    }

    private void rentList() {
        rentList.getItems().setAll(rents);
    }

    // -------- Create new item tab -------------------

    private void categoryCheck() {
        if (!notNull(chbCreateItem)) {
            return;
        }
        Category c = chbCreateItem.getSelectionModel().getSelectedItem();
        if (c.getType() == ItemType.BASKET) {
            showhideFields(true);
        } else {
            showhideFields(false);
        }
    }

    @FXML
    private void btnCreateItem() throws NumberFormatException, Exception {
        String errorString = "";

        // Checking if postal is present and a number.
        String txfcreateName = txfCreateName.getText().trim();
        String txfcreatePrice = txfCreatePrice.getText().trim();
        int createName = -1;
        int createPrice = -1;

        try {
            createName = Integer.parseInt(txfcreateName);
        } catch (NumberFormatException ex) {
        }

        if (txfcreateName.length() == 0) {
            errorString += "Varen skal have et navn.";
        }

        if (createName > 0) {
            errorString += "\nNavn må ikke være et tal.\n";
        }

        try {
            createPrice = Integer.parseInt(txfcreatePrice);
        } catch (NumberFormatException ex) {
        }

        if (createPrice < 0) {
            errorString += "\nPrisen skal være et tal.";
        }

        if (txfcreatePrice.length() == 0) {
            errorString += "\nVaren skal have en pris.";
        }

        // If any errors added to errorString, creates alertwindow with all the
        // errors.
        if (errorString.length() != 0) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error");
            alert.setContentText(errorString);
            alert.show();

            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            stage.setAlwaysOnTop(true);
            stage.toFront();
            return;
        } else if (errorString.length() == 0) {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Success");
            alert.setContentText("Vare: " + txfCreateName.getText().trim().toUpperCase() +
                    " oprettet som en " + chbCreateItem.getSelectionModel().getSelectedItem().getName().toUpperCase()
                    + " til en pris af " + txfCreatePrice.getText().trim().toUpperCase() + " DKK");
            alert.show();
            rentList();
            ItemPrices p = service.Item_CreatorDKK_SamePrice(txfCreateName.getText().trim(),
                    chbCreateItem.getSelectionModel().getSelectedItem(),
                    Double.parseDouble(txfCreatePrice.getText().trim()));
            chbPriceList.getSelectionModel().getSelectedItem().addPrices(p);

            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            stage.setAlwaysOnTop(true);
            stage.toFront();
            return;
        }
    }

    @FXML
    private void chbCreateItemCategories() {
        chbCreateItem.getItems().setAll(storage.getCategories());
        chbCreateItem.getSelectionModel().selectFirst();
        chbCreateItem.setOnAction((event) -> {
            categoryCheck();
        });
    }

    @FXML
    private void chbCategoryType() {
        chbCategoryType.getItems().setAll(storage.getTypeList());
        chbCategoryType.getSelectionModel().selectFirst();

    }

    // -- Hide/show buttons when basket is chosen (CreateItem) --

    private void showhideFields(boolean b) {
        lblBeerAmount.setVisible(b);
        lblBeerAmount.setDisable(!b);
        lblGlassAmount.setVisible(b);
        lblGlassAmount.setDisable(!b);
        btnMore1.setVisible(b);
        btnMore1.setDisable(!b);
        btnMore2.setVisible(b);
        btnMore2.setDisable(!b);
        btnFewer1.setVisible(b);
        btnFewer1.setDisable(!b);
        btnFewer2.setVisible(b);
        btnFewer2.setDisable(!b);
        beerCount.setVisible(b);
        beerCount.setDisable(!b);
        glassCount.setVisible(b);
        glassCount.setDisable(!b);
    }
    // ----------------- Category Creator Methods ----------------------

    @FXML
    private void btnCreateCategory() throws Exception {
        if (txfCreateCategory.getText().trim().length() == 0) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Fejl");
            alert.setContentText("Kategori skal have et navn.");
            alert.showAndWait();
        } else {
            service.categoryCreator(txfCreateCategory.getText().trim(),
                    chbCategoryType.getSelectionModel().getSelectedItem());
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Succes");
            alert.setContentText("Kategori oprettet");
            alert.show();
            txfCreateCategory.clear();
            updateControls();

        }
    }

    // ------------------- Statistics Pane Methods --------------------

    @FXML
    private void dtpStatistics() {
        lwStatistics.getItems().setAll(service.getSalesByDay(dtpStatistics.getValue()));
    }
}

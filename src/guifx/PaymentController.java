package guifx;

import java.net.URL;
import java.text.DecimalFormat;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;
import model_Order.ActiveOrder;
import model_Price.VALUTA_TYPE;
import storage.Storage;

public class PaymentController implements Initializable {

    // ------------------ Init fields ----------------------------
    @FXML
    private RadioButton            mobilePay, Dankort, Kontant, Klippekort;

    @FXML
    private Button                 btnPay;

    @FXML
    private TextField              txfPrice;

    @FXML
    private ToggleGroup            payment;

    private ActiveOrder            ao    = BarController.ao;

    private double                 price = ao.getPrice();

    private DecimalFormat          dc    = new DecimalFormat(".###");

    @FXML
    private ChoiceBox<VALUTA_TYPE> chbValuta;

    // ------------------------------------------------------------

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        fillChbValuta();
        txfPrice.setText(dc.format(price) + " " + ao.getValutaType());
        chbValuta();
    }

    @FXML
    private void chbValuta() {
        chbValuta.setOnAction((event) -> {
            if (chbValuta.getValue() != null) {
                VALUTA_TYPE v = chbValuta.getValue();
                price = ao.valutaConversion(v);

                txfPrice.setText(dc.format(price) + " " + ao.getValutaType());
            }
        });

    }

    @FXML
    private void onPay(ActionEvent event) {

        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Betaling gennemført!");
        alert.setContentText(
                "Betaling af " + ao.getPrice() + " " + ao.getValutaType() + " er gennemført uden problemer.");
        alert.showAndWait();
        Stage stage = (Stage) btnPay.getScene().getWindow();
        stage.close();

    }

    private void fillChbValuta() {
        chbValuta.getItems().clear();
        for (VALUTA_TYPE v : Storage.getInstance().getValutas()) {
            if (!v.equals(VALUTA_TYPE.VOUCHER_MARK)) {
                chbValuta.getItems().add(v);
            }
        }
        chbValuta.getSelectionModel().select(VALUTA_TYPE.DKK_Normal);
    }

}

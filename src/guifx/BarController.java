package guifx;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import model_Items.Category;
import model_Items.ItemType;
import model_Items.Keg;
import model_Order.ActiveOrder;
import model_Order.Order;
import model_Order.OrderList;
import model_Price.ItemPrices;
import model_Price.PriceList;
import model_Price.VALUTA_TYPE;
import service.Service;

public class BarController implements Initializable {

    // Add all the scenebuilder stuff!
    private Service              service = Service.getInstance();

    ObservableList<Category>     categoryList;

    @FXML
    private Pane                 layoutPane, fredagsbarPane, btnTourOrder;

    @FXML
    private Button               barSystemBtn, lagerSystemBtn, fredagsbarTabBtn, butikTabBtn, btnMinus, btnPlus,
            btnAdd, btnRemove, btnPay, btnPlusStore, btnMinusStore, btnAddStore, btnRemoveStore, btnPayStore, btnBack;

    // ----------------- Tour Fields ---------------------
    @FXML
    private Button               btnAdultFewer, btnAdultMore, btnStudentFewer, btnStudentMore, btnChildFewer,
            btnChildMore, btnOrderTour;

    @FXML
    private TextField            txfAdultAmount, txfStudentAmount, txfChildAmount, txfKegSize;

    @FXML
    private DatePicker           dtpTourDate;

    @FXML
    private Label                lblTourDate, lblTour, lblAdultAmount, lblStudentAmount, lblChildAmount, lblKegSize;

    @FXML
    private RadioButton          rbtnDay, rbtnEvening;

    @FXML
    private BorderPane           borderPane;

    @FXML
    private ToggleGroup          rbtnTimeGroup;

    @FXML
    private ImageView            logo, logo1, background;

    @FXML
    private TabPane              tabPane;

    @FXML
    private TextField            counterBar, totalPrice, totalPriceEUR, counterBarStore, totalPriceStore;

    @FXML
    private ListView<ItemPrices> lwSelection, lwSelectionStore;

    @FXML
    private ListView<OrderList>  lwOrder, lwOrderStore;

    @FXML
    private ChoiceBox<Category>  chbCategories, chbCategoriesStore;

    @FXML
    ChoiceBox<PriceList>         chbPriceListStore;

    private Order                order   = new Order();

    public static ActiveOrder    ao;

    // Integer for the counter.
    int                          count   = 1, storeCount = 1;

    // Counters for Tour Pane
    int                          adultAmount, studentAmount, childAmount;

    // ----------------------- Init -----------------------------

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        newStart();
    }

    // ------------- General BarController Methods ---------------

    private void newStart() {
        order = new Order();
        updateControls();
        totalPrice.clear();
        totalPriceStore.clear();

    }

    private void updateControls() {
        chbCategories();
        getFullSelection();
        chbPriceListStore();
        chbCategoriesStore();
        getStoreSelection();
        amountChangedTour();
        showOrderSelection();
        totalPrice();
    }

    @FXML
    private void btnBack() {
        Stage stage = (Stage) btnBack.getScene().getWindow();
        stage.close();
    }

    private boolean notNull(ChoiceBox<?> c) {
        return c.getSelectionModel().getSelectedItem() != null;
    }

    // ---------------- Selection Methods -----------------------

    @FXML
    private void chbCategories() {
        chbCategories.getItems().setAll(service.getNonEmptyCategories(service.getBarPriceList()));
        chbCategories.getSelectionModel().selectFirst();
        chbCategories.setOnAction((event) -> {

            getFullSelection();
        });
    }

    @FXML
    private void getFullSelection() {
        Category c = chbCategories.getSelectionModel().getSelectedItem();
        if (c != null) {
            lwSelection.getItems().setAll(service.getBarIPbyCategory(c));
        }
    }

    @FXML
    private void showOrderSelection() {
        lwOrder.getItems().setAll(order.getOrderList());
        lwOrderStore.getItems().setAll(order.getOrderList());

    }
    // -------------------- StorePane Methods ---------------------

    @FXML
    private void getStoreSelection() {
        lwSelectionStore.getItems().clear();
        Category c = chbCategoriesStore.getSelectionModel().getSelectedItem();
        PriceList p = chbPriceListStore.getSelectionModel().getSelectedItem();
        if (c != null && p != null) {
            lwSelectionStore.getItems()
                    .setAll(service.getShopIPbyCategory(p, c));

        }
    }

    @FXML
    private void chbCategoriesStore() {
        if (notNull(chbPriceListStore)) {
            chbCategoriesStore.setDisable(false);
            chbCategoriesStore.getItems()
                    .setAll(service.getNonEmptyCategories(chbPriceListStore.getSelectionModel().getSelectedItem()));
            chbCategoriesStore.getSelectionModel().selectFirst();
            if (chbCategoriesStore.getItems().isEmpty()) {
                chbCategoriesStore.setDisable(true);
            }
            chbCategoriesStore.setOnAction((event) -> {
                if (notNull(chbCategoriesStore)) {
                    changeKegVisibility(false);
                    if (chbCategoriesStore.getSelectionModel().getSelectedItem().getType().equals(ItemType.KEG)) {
                        changeKegVisibility(true);
                    }
                    getStoreSelection();
                }

            });
        }
    }

    @FXML
    private void chbPriceListStore() {
        chbPriceListStore.getItems().setAll(service.getNonBarPriceList());
        chbPriceListStore.getSelectionModel().selectFirst();
        chbPriceListStore.setOnAction((event) -> {
            chbCategoriesStore();
            getStoreSelection();
        });
    }

    private void changeKegVisibility(boolean b) {
        txfKegSize.setVisible(b);
        lblKegSize.setVisible(b);
        txfKegSize.setDisable(!b);
        lblKegSize.setDisable(!b);
        txfKegSize.setEditable(b);
    }

    // -------------- Total Price ------------------------
    private void totalPrice() {
        totalPrice.setText("" + order.fullCost());
        totalPriceStore.setText("" + order.fullCost());
    }

    // -------------- Amount counter ---------------------
    private void counterBar() {
        counterBar.setText(Integer.toString(count));
        counterBarStore.setText(Integer.toString(storeCount));

    }

    @FXML
    private void btnMinus() {
        if (count > 1) {
            count--;
            counterBar();
        }
    }

    @FXML
    private void btnPlus() {
        count++;
        counterBar();
    }

    @FXML
    private void btnMinusStore() {
        if (storeCount > 1) {
            storeCount--;
            counterBar();
        }
    }

    @FXML
    private void btnPlusStore() {
        storeCount++;
        counterBar();
    }

    private void amountResetToOne() {
        this.count = 1;
        this.storeCount = 1;
        counterBar();
    }

    // ----------------- Pay Button Methods --------------
    @FXML
    private void onPayment(ActionEvent event) {
        if (order.getOrderList().isEmpty()) {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Ingen vare");
            alert.setContentText("Der er ikke tilføjet nogle vare til ordren!");
            alert.show();
        } else {
            try {
                ao = new ActiveOrder(order, LocalDateTime.now());
                FXMLLoader root = new FXMLLoader(getClass().getResource("/guifx/paymentWindow.fxml"));
                Parent root1 = (Parent) root.load();
                Stage stage = new Stage();
                stage.setScene(new Scene(root1));
                stage.setResizable(false);
                stage.centerOnScreen();
                stage.sizeToScene();
                stage.showAndWait();
                service.orderPayed(ao);
                ao = null;
                newStart();

            } catch (IOException e) {
                System.out.println("Bar Controller GUI : ERROR: " + e + "Error cause: " + e.getCause());
                System.out.println("Bar Controller GUI : ERROR: " + e);
            }
        }
    }

    // ----------------- Misc Button Methods -----------------
    @FXML
    private void btnAdd() {
        if (lwSelection.getSelectionModel().getSelectedItem() != null) {
            OrderList o = new OrderList(lwSelection.getSelectionModel().getSelectedItem(), count);
            if (order.getOrderListByItem(o.getItem()) != null) {
                order.getOrderListByItem(o.getItem()).addAmount(count);
            } else {
                order.addOrder(o);
            }
            showOrderSelection();
            totalPrice();
            amountResetToOne();
        } else {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Advarsel:");
            alert.setContentText("Der skal vælges en enhed før der kan tilføjes.");
            alert.show();
        }
    }

    @FXML
    private void btnAddStore() {
        Alert alert = new Alert(AlertType.WARNING);
        try {
            if (lwSelectionStore.getSelectionModel().getSelectedItem() != null) {
                ItemPrices i = new ItemPrices(lwSelectionStore.getSelectionModel().getSelectedItem());
                if (chbCategoriesStore.getSelectionModel().getSelectedItem().getType().equals(ItemType.KEG)) {
                    Keg k = (Keg) i.getItem();
                    if (k.getSize() > 0) {
                        int temp = Integer.parseInt(txfKegSize.getText());
                        i.setPrice(VALUTA_TYPE.DKK_Normal, (i.getPrice(VALUTA_TYPE.DKK_Normal) / k.getSize())
                                * temp);

                        k.setSize(temp);
                    }
                }
                OrderList o = new OrderList(i, storeCount);
                if (order.getOrderListByItem(o.getItem()) != null) {
                    order.getOrderListByItem(o.getItem()).addAmount(storeCount);
                } else {
                    order.addOrder(o);
                }

                showOrderSelection();
                totalPrice();
                amountResetToOne();
            } else {

                alert.setTitle("Advarsel:");
                alert.setContentText("Der skal vælges en enhed før der kan tilføjes.");
                alert.show();
            }
        } catch (NumberFormatException e) {
            alert.setAlertType(AlertType.ERROR);
            alert.setContentText("Fustage størrelse skal skrives med tal");
        }
    }

    @FXML
    private void btnRemove() {
        if (order.getOrderList().isEmpty() == true) {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Advarsel : ");
            alert.setContentText("Intet at fjerne - tilføj noget til ordren først.");
            alert.show();
        } else {
            order.removeOrder(lwOrder.getSelectionModel().getSelectedItem());
            showOrderSelection();
            totalPrice();
            amountResetToOne();
        }
    }

    @FXML
    private void btnRemoveStore() {
        if (order.getOrderList().isEmpty() == true) {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Advarsel : ");
            alert.setContentText("Intet at fjerne - tilføj noget til ordren først.");
            alert.show();
        } else {
            order.removeOrder(lwOrderStore.getSelectionModel().getSelectedItem());
            showOrderSelection();
            totalPrice();
            amountResetToOne();
        }
    }
    // ---------------------------- Tour Pane ----------------------------------

    private void amountChangedTour() {
        txfAdultAmount.setText("" + adultAmount);
        txfStudentAmount.setText("" + studentAmount);
        txfChildAmount.setText("" + childAmount);

    }

    @FXML
    private void btnAdultMore() {
        adultAmount++;
        amountChangedTour();
    }

    @FXML
    private void btnAdultFewer() {
        if (adultAmount > 0) {
            adultAmount--;
        }
        amountChangedTour();
    }

    @FXML
    private void btnStudentMore() {
        studentAmount++;
        amountChangedTour();
    }

    @FXML
    private void btnStudentFewer() {
        if (studentAmount > 0) {
            studentAmount--;
        }
        amountChangedTour();
    }

    @FXML
    private void btnChildMore() {
        childAmount++;
        amountChangedTour();
    }

    @FXML
    private void btnChildFewer() {
        if (childAmount > 0) {
            childAmount--;
        }
        amountChangedTour();
    }

    @FXML
    private void btnOrderTour() {
        boolean inEvening = rbtnEvening.isSelected();
        Order o = service.placeTourOrder(adultAmount, studentAmount, childAmount, LocalDate.of(2017, 10, 9), inEvening);

        new ActiveOrder(o, LocalDateTime.now());

    }

}

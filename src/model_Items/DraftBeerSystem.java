package model_Items;

import java.time.LocalDate;
import java.util.ArrayList;

import model_Order.OrderList;
import model_Price.ItemPrices;

public class DraftBeerSystem extends Item {

    private int                  tabsAmount   = 0;
    private double               price        = 0;

    private ArrayList<OrderList> kegsList     = new ArrayList<>();

    private LocalDate            startDate;
    private LocalDate            endDate;

    private boolean              withDelivery = false;
    private boolean              inUse        = false;
    private String               renter;

    public DraftBeerSystem(String name, int tabs, Category category) {
        super(name, category);
        this.tabsAmount = tabs;
        setName();

    }

    public DraftBeerSystem(String name, Category category) {
        super(name, category);
    }

    private void setName() {
        if (tabsAmount < 1) {
            try {
                throw new Exception("Non-valid tabs amount");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (tabsAmount == 1) {
            this.price = 250;
            super.setName("Anlæg m. " + tabsAmount + " hane.");
        } else if (tabsAmount == 2) {
            this.price = 400;
            super.setName("Anlæg m. " + tabsAmount + " haner.");
        } else if (tabsAmount > 2) {
            this.price = 500;
            super.setName("Anlæg m. " + tabsAmount + " haner.");
        }
    }

    public void setRenter(String renter) {
        this.renter = renter;
        this.inUse = true;
    }

    public int getTabs() {
        return tabsAmount;
    }

    public void setTabs(int amount) {
        this.tabsAmount = amount;
    }

    public boolean isInUse() {
        return inUse;
    }

    public void returnedEquipment() {
        this.inUse = false;
        this.renter = null;
    }

    public boolean withDelivery() {
        return withDelivery;
    }

    public void setDelivery(boolean setDelivery) {
        this.withDelivery = setDelivery;
    }

    public void setStartDate(LocalDate date) {
        this.startDate = date;
    }

    public void setEndDate(LocalDate date) {
        this.endDate = date;
    }

    public void returned() {
        this.startDate = null;
        this.endDate = null;
        this.inUse = false;
        this.withDelivery = false;
        this.kegsList.clear();
    }

    public void addKeg(ItemPrices itemPrice, int amount) {
        OrderList kegsList = new OrderList(itemPrice, amount);
        this.kegsList.add(kegsList);
    }

    public boolean removeKeg(OrderList orderList) {
        if (kegsList.contains(orderList)) {
            kegsList.remove(orderList);
            return true;
        } else {
            return false;
        }
    }

    public double getFullCost() {
        double sum = price;
        for (OrderList o : kegsList) {
            sum += o.getPrice();
        }
        return sum;
    }

}

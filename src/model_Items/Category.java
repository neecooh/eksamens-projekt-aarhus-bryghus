package model_Items;

public class Category {

    private String   name;
    private ItemType type;
    private boolean  voucherPayable = false;
    private int      voucherAmount  = 0;

    public Category(String name, ItemType type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public ItemType getType() {
        return type;
    }

    @Override
    public String toString() {
        return name;
    }

    public boolean isVoucherPayable() {
        return voucherPayable;
    }

    public int getVoucherAmount() {
        return voucherAmount;
    }

    public void VoucherPayable(boolean b) {
        if (!b) {
            voucherAmount = 0;
        }
        this.voucherPayable = b;
    }

    public void setVoucherAmount(int i) {
        if (i > 0) {
            this.voucherPayable = true;
        }
        this.voucherAmount = i;
    }

}

package model_Items;

import java.util.ArrayList;

public class Basket extends Item {

    private ArrayList<Item> beerList    = new ArrayList<>();
    private ArrayList<Item> glassList   = new ArrayList<>();
    private int             beerAmount  = 0;
    private int             glassAmount = 0;

    public Basket(String name, Category category, int beerAmount) {
        super(name, category);
        this.beerAmount = beerAmount;
    }

    public Basket(String name, Category category, int beerAmount, int glassAmount) {
        this(name, category, beerAmount);
        this.glassAmount = glassAmount;
    }

    public boolean addBeer(Item beer, int amount) {
        if (beer.getItemType() != ItemType.BEER) {
            return false;
        }
        if (beerList.size() + amount > beerAmount) {
            return false;
        } else {
            for (int i = 0; i < amount; i++) {
                beerList.add(beer);
            }
            return true;
        }
    }

    public void setGlassAmount(int amount) {
        this.glassAmount = amount;
    }

    public boolean addGlass(Item glass, int amount) {
        if (glass.getItemType() != ItemType.GLASS) {
            return false;
        }
        if (glassList.size() + amount > glassAmount) {
            return false;
        } else {
            for (int i = 0; i < amount; i++) {
                glassList.add(glass);
            }
            return true;
        }
    }

    public boolean notFilled() {
        if (beerList.size() != beerAmount || glassAmount != glassList.size()) {
            return true;
        } else {
            return false;
        }
    }

}

package model_Items;

public class Keg extends Item {

    private int size = -1;

    public Keg(Item i, int sizeAmount) {
        super(i);
        this.size = sizeAmount;
    }

    public Keg(String name, Category category, int sizeAmount) {
        super(name, category);
        this.size = sizeAmount;
    }

    public void setSize(int amount) {
        this.size = amount;
    }

    public int getSize() {
        return size;
    }

    @Override
    public String toString() {
        return super.getName() + ", " + size + " l. ";
    }

}

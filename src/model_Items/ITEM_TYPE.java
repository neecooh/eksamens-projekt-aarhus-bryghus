package model_Items;

import storage.Storage;

public enum ItemType {

    //@formatter:off
    BEER("Øl"),
    VOUCHER("Klippekort"),
    DBS("Fadølsanlæg"),
    KEG("Fustage"),
    KEG_EXTRA("Fustage Tilbehør"),
    WEARABLE("Beklædning"),
    GLASS("Glas"),
    TOUR("Tour"),
    SNACKS("Snacks"),
    SPIRIT("Spiritus"),
    NonALCO("Forfriskninger"),
    BASKET("Gavekurt"),
    MALT("Malt"),
    CARBON("Kulsyre"),
    ;
    //@formatter:on

    private final String name;

    private ItemType(String name) {
        this.name = name;
        Storage.getInstance().addType(this);
    }

    @Override
    public String toString() {
        return name;
    }

}

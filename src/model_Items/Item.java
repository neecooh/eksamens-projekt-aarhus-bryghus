package model_Items;

public class Item {

    private String   name;
    private Category category;

    public Item(String name, Category category) {
        this.name = name;
        this.category = category;
    }

    public Item(Item i) {
        this.name = i.getName();
        this.category = i.getCategory();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getCategory() {
        return category;
    }

    public ItemType getItemType() {
        return category.getType();
    }

    @Override
    public String toString() {
        return name;
    }

}

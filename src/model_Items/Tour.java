package model_Items;

import java.time.LocalDate;

public class Tour extends Item {

    private LocalDate date;
    private boolean   inEvening = false;
    private boolean   isAdult   = true;
    private boolean   isStudent = false;
    private boolean   isChild   = false;

    public Tour(String name, Category category, LocalDate date) {
        super(name, category);
        this.date = date;
    }

    public Tour(Item i) {
        super(i.getName(), i.getCategory());
    }

    public Tour(String name, Category category) {
        super(name, category);
    }

    public LocalDate getDate() {
        return this.date;
    }

    public boolean isAdult() {
        return isAdult;
    }

    public boolean isStudent() {
        return isStudent;
    }

    public boolean isChild() {
        return isChild;
    }

    public void setChild() {
        isAdult = false;
        isStudent = false;
        isChild = true;
    }

    public void setAdult() {
        isAdult = true;
        isStudent = false;
        isChild = false;
    }

    public void setStudent() {
        isAdult = false;
        isStudent = true;
        isChild = false;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public boolean isInEvening() {
        return inEvening;
    }

    public void setInEvening(boolean b) {
        this.inEvening = b;
    }

}

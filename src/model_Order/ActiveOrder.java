package model_Order;

import java.time.LocalDateTime;
import java.util.ArrayList;

import model_Price.ItemPrices;
import model_Price.VALUTA_TYPE;
import service.Service;

public class ActiveOrder {

    private Order         order;
    private double        price;
    private VALUTA_TYPE   valutaType = VALUTA_TYPE.DKK_Normal;
    private LocalDateTime date;
    private Service       service    = Service.getInstance();

    public ActiveOrder(Order order, LocalDateTime date) {
        this.order = order;
        service.orderCleaner(order);
        this.price = order.fullCost();
        this.date = date;
    }

    public double getPrice() {
        return price;
    }

    public void payItemWithVoucher(int amount) {
        for (OrderList o : order.getOrderList()) {
            ItemPrices ph = o.getItemPrices();
            if (ph.containsPrice(VALUTA_TYPE.VOUCHER_MARK)) {
                for (int i = 0; i < o.getAmount(); i++) {
                    if (ph.getPrice(VALUTA_TYPE.VOUCHER_MARK) <= amount) {
                        amount -= ph.getPrice(VALUTA_TYPE.VOUCHER_MARK);
                        service.removeOneAmountFromOrderList(o);
                    }
                }
            }
        }

        service.orderCleaner(order);
    }

    public LocalDateTime getDate() {
        return this.date;
    }

    public ArrayList<OrderList> getItemList() {
        return order.getOrderList();
    }

    public VALUTA_TYPE getValutaType() {
        return valutaType;
    }

    public double conversionRate() {

        if (valutaType == VALUTA_TYPE.BRITISH_POUNDS) {
            return 8.6;
        } else if (valutaType == VALUTA_TYPE.DKK_Normal) {
            return 1.0;
        } else if (valutaType == VALUTA_TYPE.EURO) {
            return 7.45;
        } else if (valutaType == VALUTA_TYPE.US_DOLLARS) {
            return 6.04;
        } else {
            throw new NullPointerException("No conversion rate for that valuta avaliable");
        }
    }

    /*
     * If price is not in DKK, returns the price to DKK and then converges the
     * price
     */
    public double valutaConversion(VALUTA_TYPE valutaType) {
        double conversion = 0.0;

        if (valutaType == VALUTA_TYPE.BRITISH_POUNDS) {
            conversion = 8.6;
        } else if (valutaType == VALUTA_TYPE.DKK_Normal) {
            conversion = 1.0;
        } else if (valutaType == VALUTA_TYPE.EURO) {
            conversion = 7.45;
        } else if (valutaType == VALUTA_TYPE.US_DOLLARS) {
            conversion = 6.04;
        }

        price = price * conversionRate() / conversion;
        this.valutaType = valutaType;
        return price;
    }

    public int getFullVoucherCost() {
        int temp = 0;
        for (OrderList o : order.getOrderList()) {
            ItemPrices ph = o.getItemPrices();
            if (ph.containsPrice(VALUTA_TYPE.VOUCHER_MARK)) {
                temp += ph.getPrice(VALUTA_TYPE.VOUCHER_MARK) * o.getAmount();
            }
        }

        return temp;
    }

    public double calculateDiscountedPrice(int amount) {
        return price * (1 - ((double) amount / 100));
    }

    @Override
    public String toString() {
        return "Order nr. " + hashCode() + ", Full price: " + price + " " + valutaType;
    }
}

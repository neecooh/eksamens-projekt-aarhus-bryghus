package model_Order;

import java.util.ArrayList;

import model_Items.Item;
import model_Price.ItemPrices;

public class Order {

    private ArrayList<OrderList> orderList = new ArrayList<>();

    public Order() {
    }

    public void addOrder(OrderList o) {
        orderList.add(o);
    }

    public boolean removeOrder(OrderList o) {
        if (orderList.contains(o)) {
            orderList.remove(o);
            return true;
        } else {
            return false;
        }
    }

    public ArrayList<OrderList> getOrderList() {
        return orderList;
    }

    public OrderList addOrder(ItemPrices ip, int amount) {
        OrderList o = new OrderList(ip, amount);
        orderList.add(o);

        return o;
    }

    /*
     * Returns fullCost in DKK, due to OrderList.getPrice() is DKK by default
     */
    public double fullCost() {
        double sum = 0;
        for (OrderList o : orderList) {
            sum += o.getPrice();
        }
        return sum;
    }

    public boolean containsOrderList(OrderList o) {
        if (orderList.contains(o)) {
            return true;
        }
        return false;
    }

    public OrderList getOrderListByItem(Item i) {
        for (OrderList o : orderList) {
            if (o.getItem() == i) {
                return o;
            }
        }
        return null;
    }

    public boolean missingItemsInBasket() {
        boolean b = false;
        for (OrderList o : orderList) {
            b = o.basketNotFilled();
        }
        return b;
    }

    public int voucherAmount() {
        int temp = 0;
        for (OrderList o : orderList) {
            if (o.getCategory().isVoucherPayable()) {
                temp += o.getVoucherAmount();
            }
        }
        return temp;
    }

    @Override
    public String toString() {
        return "Ordre størrlse: " + orderList.size() + ", fuld pris: " + fullCost();
    }

}

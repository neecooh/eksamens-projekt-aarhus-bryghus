package model_Order;

import model_Items.Basket;
import model_Items.Category;
import model_Items.Item;
import model_Items.ItemType;
import model_Price.ItemPrices;
import model_Price.VALUTA_TYPE;

public class OrderList {

    private Item       item;
    private ItemPrices list;
    private int        amount = 1;

    public OrderList(ItemPrices itemPrices) {
        this.item = itemPrices.getItem();
        this.list = itemPrices;
    }

    public OrderList(ItemPrices itemPrices, int amount) {
        this(itemPrices);
        this.amount = amount;
    }

    public Item getItem() {
        return item;
    }

    public int getAmount() {
        return amount;
    }

    public ItemPrices getItemPrices() {
        return list;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void addAmount(int amount) {
        this.amount += amount;
    }

    /*
     * Will always return price for OrderList in DKK
     */
    public double getPrice() {
        return list.getPrice(VALUTA_TYPE.DKK_Normal) * amount;
    }

    public boolean basketNotFilled() {
        if (getItemType() == ItemType.BASKET) {
            Basket b = (Basket) getItem();
            return b.notFilled();
        }
        return false;
    }

    public ItemType getItemType() {
        return item.getItemType();
    }

    public Category getCategory() {
        return item.getCategory();
    }

    public int getVoucherAmount() {
        return item.getCategory().getVoucherAmount() * amount;
    }

    @Override
    public String toString() {
        return item.getName() + ": " + amount + " stk.";
    }
}

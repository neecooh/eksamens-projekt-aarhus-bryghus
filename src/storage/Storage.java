package storage;

import java.util.ArrayList;
import java.util.Calendar;

import model_Items.Category;
import model_Items.DraftBeerSystem;
import model_Items.Item;
import model_Items.ItemType;
import model_Order.ActiveOrder;
import model_Price.ItemPrices;
import model_Price.PriceList;
import model_Price.VALUTA_TYPE;

public class Storage {

    static Storage                     storage;

    private ArrayList<PriceList>       priceLists        = new ArrayList<>();
    private ArrayList<Category>        categoryList      = new ArrayList<>();
    private ArrayList<Item>            itemList          = new ArrayList<>();
    private ArrayList<DraftBeerSystem> rentalEquipment   = new ArrayList<>();

    private ArrayList<ActiveOrder>     savedActiveOrders = new ArrayList<>();

    private ArrayList<ItemType>        typeList          = new ArrayList<>();
    private ArrayList<VALUTA_TYPE>     valutaList        = new ArrayList<>();

    private Storage() {
    }

    public static Storage getInstance() {
        if (storage == null) {
            storage = new Storage();
        }
        return storage;
    }

    public ArrayList<VALUTA_TYPE> getValutas() {
        return valutaList;
    }

    public void addValuta(VALUTA_TYPE type) {
        valutaList.add(type);
    }

    public ArrayList<DraftBeerSystem> getDBSList() {
        return rentalEquipment;
    }

    public void addDBSList(DraftBeerSystem d) {
        rentalEquipment.add(d);
    }

    public ArrayList<PriceList> getPricesList() {
        return priceLists;
    }

    public void addPricesList(PriceList p) {
        priceLists.add(p);
    }

    public ArrayList<Item> getItemsList() {
        return itemList;
    }

    public ArrayList<Category> getCategories() {
        return new ArrayList<>(categoryList);
    }

    public Category getCategory(ItemType i) {
        for (Category c : categoryList) {
            if (c.getType() == i) {
                return c;
            }
        }
        return null;
    }

    public ArrayList<ItemType> getTypeList() {
        return typeList;
    }

    public void addType(ItemType type) {
        typeList.add(type);
    }

    public ArrayList<Item> getItemsByType(ItemType type) {
        ArrayList<Item> list = new ArrayList<>();
        for (Item i : itemList) {
            if (i.getCategory().getType() == type) {
                list.add(i);
            }
        }
        return list;
    }

    public void addCategory(Category c) {
        categoryList.add(c);

    }

    public void addItem(Item e) {
        itemList.add(e);
    }

    public ItemPrices getTourPrice() {
        for (PriceList p : priceLists) {
            for (ItemPrices i : p.getList()) {
                if (i.getType() == ItemType.TOUR) {
                    return i;
                }
            }
        }
        return null;
    }

    // ------------- Order Methods ----------------
    public void saveOrder(ActiveOrder order) {
        Calendar currentDay = Calendar.getInstance();
        Calendar now = Calendar.getInstance();

        currentDay.set(Calendar.HOUR_OF_DAY, 0);
        currentDay.set(Calendar.MINUTE, 0);
        currentDay.set(Calendar.SECOND, 0);
        currentDay.set(Calendar.MILLISECOND, 0);

        if (now.getTimeInMillis() - currentDay.getTimeInMillis() >= 86400000) {
            this.savedActiveOrders = new ArrayList<>();
        }

        savedActiveOrders.add(order);
    }

    public ArrayList<ActiveOrder> getSavedActiveOrders() {
        return this.savedActiveOrders;
    }

}
